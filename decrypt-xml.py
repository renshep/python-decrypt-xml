#!/usr/bin/python3

import os
xml = os.environ.get('XML_FILE',False)
key = os.environ.get('KEY_FILE',False)

if xml == False:
    exit(1)

if key == False:
    exit(2)

from lxml import etree
import xmlsec

manager = xmlsec.KeysManager()
key = xmlsec.Key.from_file(key, xmlsec.constants.KeyDataFormatPem)
manager.add_key(key)
enc_ctx = xmlsec.EncryptionContext(manager)
rootxml = etree.parse(xml)
root = rootxml.getroot()
enc_data = xmlsec.tree.find_node(root, "EncryptedData", xmlsec.constants.EncNs)
decrypted = enc_ctx.decrypt(enc_data)

print(etree.tostring(decrypted))
